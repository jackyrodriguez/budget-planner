# coding=utf-8
from flask.ext.sqlalchemy import SQLAlchemy
from werkzeug import generate_password_hash, check_password_hash
import datetime, time
 
db = SQLAlchemy()
 
class User(db.Model):
  __tablename__ = 'users'
  uid = db.Column(db.Integer, primary_key = True)
  firstname = db.Column(db.String(100))
  lastname = db.Column(db.String(100))
  email = db.Column(db.String(120), unique=True)
  password = db.Column(db.String(54))
   
  def __init__(self, firstname, lastname, email, password):
    self.firstname = firstname.title()
    self.lastname = lastname.title()
    self.email = email.lower()
    self.set_password(password)
     
  def set_password(self, password):
    self.password = generate_password_hash(password)
   
  def check_password(self, password):
    return check_password_hash(self.password, password)


class Expense(db.Model):
  __tablename__= 'expense'
  eid = db.Column(db.Integer, primary_key = True)
  category = db.Column(db.String(200))
  amount = db.Column(db.String(50))
  uid = db.Column(db.Integer)
  date_of_expense = db.Column(db.DateTime)
  date_logs = db.Column(db.DateTime)

  def __init__(self, category, amount, uid, date_of_expense, date_logs):
    self.category = category
    self.amount = amount
    self.uid = uid
    if date_of_expense is None:
      date_of_expense = datetime.datetime.now()
    self.date_of_expense = date_of_expense
    if date_logs is None:
      date_logs = datetime.datetime.now()
    self.date_logs = date_logs

  #def __repr__(self):
    #return "<Expense('%s','%s', '%s')>" % (self.category, self.amount, self.uid)


