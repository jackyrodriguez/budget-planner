from intro_to_flask import app
from flask import render_template, request, flash, session, url_for, redirect
from forms import AddExpense, SignupForm, SigninForm, EditExpense
from flask.ext.mail import Message, Mail
from models import db, User, Expense

mail = Mail()

@app.route('/')
def home():
  return render_template('home.html')

#comment here
@app.route('/report/<expenseid>')
def report(expenseid):
  user_expense = Expense.query.filter_by(eid=expenseid).first()
  form = EditExpense()
  if request.method == 'POST':
      if form.validate() == False:
        flash('All fields are required.')
        return render_template('expense.html', form=form)
      else:
        editexpense = Expense(form.categories.data, form.amount.data, form.user_id.data, None, None)
        db.session.add(editexpense)
        db.session.commit()
  elif request.method == 'GET':
  #return render_template('report.html', form=form)
    return render_template('report.html', user_expense=form)


@app.route('/new')
def new():
  user = User.query.filter_by(uid=session.get('uid')).first()
  budget_list = Expense.query.filter_by(uid=user.uid).all()
  for item in budget_list:
    print item
  #user_list = User.query.filter_by(user=user_list).first()
  return render_template('budget.html', budget=budget_list)

@app.route('/update', methods=['GET', 'POST'])
def update():
  form = EditExpense()
  if request.method == 'POST':
      if form.validate() == False:
        flash('All fields are required.')
        return render_template('expense.html', form=form)
      else:
        editexpense = Expense(form.categories.data, form.amount.data, form.user_id.data, None, None)
        db.session.add(editexpense)
        db.session.commit()
  elif request.method == 'GET':
    return render_template('report.html', form=form)
      #return "Updated"


@app.route('/budget')
def budget():
  budget_list = Expense.query.filter_by().all()  
  return render_template('budget.html', budget=budget_list)


@app.route('/expense', methods=['GET', 'POST'])
def expense():
  form = AddExpense()

  if 'uid' not in session:
    return redirect(url_for('signin'))

  if request.method == 'POST':
    if form.validate() == False:
      flash('All fields are required.')
      return render_template('expense.html', form=form)
    else:
      addexpense = Expense(form.categories.data, form.amount.data, session['uid'], None, None)
      db.session.add(addexpense)
      db.session.commit()
  
      msg = Message(subject='Expense Manager', sender='mydummybar@gmail.com', recipients=[form.email.data])      
      msg.body = """
      Categories: %s ; Amount: %s ;
       Description:  %s
      """ % (form.categories.data, form.amount.data, form.description.data)
      mail.send(msg)
   
      return render_template('expense.html', successed=True)
 
  elif request.method == 'GET':
    return render_template('expense.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
  form = SignupForm()
   
  if request.method == 'POST':
    if form.validate() == False:
      return render_template('signup.html', form=form)
    else:
      newuser = User(form.firstname.data, form.lastname.data, form.email.data, form.password.data)
      db.session.add(newuser)
      db.session.commit()

      session['email'] = newuser.email

      return redirect(url_for('profile'))

  elif request.method == 'GET':
    return render_template('signup.html', form=form)


@app.route('/profile')
def profile():
 
  if 'email' not in session:
    return redirect(url_for('signin'))
 
  user = User.query.filter_by(email = session['email']).first()

  session['uid'] = int(user.uid)

  if user is None:
    return redirect(url_for('signin'))
  else:
    return render_template('profile.html')


@app.route('/signin', methods=['GET', 'POST'])
def signin():
  form = SigninForm()
   
  if request.method == 'POST':
    if form.validate() == False:
      return render_template('signin.html', form=form)
    else:
      session['email'] = form.email.data
      return redirect(url_for('profile'))
                 
  elif request.method == 'GET':
    return render_template('signin.html', form=form)


@app.route('/signout')
def signout():
 
  if 'email' not in session:
    return redirect(url_for('signin'))
     
  session.pop('email', None)
  return redirect(url_for('home'))


@app.route('/clear')
def clearsess():

  session.clear()
  return redirect(url_for('home'))

@app.route('/ch')
def check():
  return render_template('home_ch.html')


